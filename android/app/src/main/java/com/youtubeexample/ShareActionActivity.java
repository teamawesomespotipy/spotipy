package com.youtubeexample;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.facebook.react.ReactActivity;
import main.java.com.youtubeexample.Helper.YoutubeHelper;

public class ShareActionActivity extends ReactActivity {
    private static final String TAG = ShareActionActivity.class.getSimpleName();

//    /**
//     * Returns the name of the main component registered from JavaScript.
//     * This is used to schedule rendering of the component.
//     */
//    @Override
//    protected String getMainComponentName() {
//        return "YoutubeExample";
//    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        onSharedIntent();
    }

    private void onSharedIntent() {
        Intent receiverdIntent = this.getIntent();
        String receivedAction = receiverdIntent.getAction();
        String receivedType = receiverdIntent.getType();
        YoutubeHelper youtubeHelper = new YoutubeHelper();

        if (receivedAction.equals(Intent.ACTION_SEND)) {

            // check mime type
            if (receivedType.startsWith("text/")) {

                String receivedText = receiverdIntent
                        .getStringExtra(Intent.EXTRA_TEXT);
                if (receivedText != null) {
                    //do your stuff
                    Context context = getApplicationContext();
                    int duration = Toast.LENGTH_LONG;
                    String videoIdFromUrl = youtubeHelper.extractVideoIdFromUrl(receivedText);
                    Toast toast = Toast.makeText(context, videoIdFromUrl, duration);
                    toast.show();
                }
            } else if (receivedType.startsWith("image/")) {
                Uri receiveUri = (Uri) receiverdIntent
                        .getParcelableExtra(Intent.EXTRA_STREAM);
                if (receiveUri != null) {
                    //do your stuff
                    //fileUri = receiveUri;// save to your own Uri object

                    Log.e(TAG, receiveUri.toString());
                }
            }

        } else if (receivedAction.equals(Intent.ACTION_MAIN)) {

            Log.e(TAG, "onSharedIntent: nothing shared");
        }
    }
}
